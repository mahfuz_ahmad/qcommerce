@extends((request()->route()->getPrefix() === '/admin')?'backend.templates.app':'frontend.templates.app')

@section('content')
    <div class="container">
        <div class="col-12">
            <h1 class="text-center mt-3"><b>Q-commerce</b></h1><hr>
            <div class="row mt-lg-5">
                <div class="col-6 m-auto">
                    <div class="card text-center">
                        <div class="card-header">
                            Success Message
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">Your Registration Successfully Done.</h5>
                            <p class="card-text">Please check your Email and active your account.</p>
                            <p class="card-text">After activate your account!</p>
                            <a href="{{ route('admin_login') }}" type="submit" class="btn btn-outline-dark">Sign in</a>
                        </div>
                        <div class="card-footer text-muted">
                            Thank you - Creatiqueit.com
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
