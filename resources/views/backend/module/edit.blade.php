@extends((request()->route()->getPrefix() === '/admin')?'backend.templates.app':'frontend.templates.app')

@section('content')
    <div class="container">
        <div class="col-12">
            <h1 class="text-center mt-3"><b>EDIT MODULE</b></h1><hr>
            <div class="row mt-lg-5">
                <div class="col-6 m-auto">
                    <div class="card text-center">
                        <div class="card-header">
                            Edit Your Module
                        </div>
                        <form action="/admin/modules/{{ $view_module->id }}" method="post">
                            @method('PUT')
                            @csrf
                            <div class="card-body">
                                <h5 class="card-title">
                                    <input type="text" name="alias" class="form-control" value="{{ $view_module->alias }}">
                                </h5>
                                <h5 class="card-title">
                                    <input type="text" name="route" class="form-control" value="{{ $view_module->route }}">
                                </h5>
                                <h5 class="card-title">
                                    <input type="text" name="icon" class="form-control" value="{{ $view_module->icon }}">
                                </h5>
                                <p class="card-text">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" value="1" name="status" {{ ($view_module->status == 1)? 'checked' : '' }}>
                                    <label class="form-check-label" for="inlineRadio1">Active</label>
                                </div>
                                <div class="form-check form-check">
                                    <input class="form-check-input" type="radio" value="0" name="status" {{ ($view_module->status == 0)? 'checked' : '' }}>
                                    <label class="form-check-label" for="inlineRadio1">Inactive</label>
                                </div>
                                </p>
                                <a href="/admin/roles" type="submit" class="btn btn-outline-dark">Back</a>
                                <button type="submit" class="btn btn-outline-dark">Update</button>
                            </div>
                        </form>
                        <div class="card-footer text-muted">
                            {{ $view_module->created_at }}
                        </div>
                    </div>
                    @if ($errors->all())
                        <div class="alert alert-danger m-auto mt-2">
                            @foreach ($errors->all() as $error)
                                <ul>
                                    <li style="padding-left: 10px">
                                        {{ $error }}
                                    </li>
                                </ul>
                            @endforeach
                        </div>
                    @endif
                    @if (session('message'))
                        <div class="alert alert-dark m-auto mt-2">
                            {{ session('message') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
