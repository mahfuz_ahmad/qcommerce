@extends((request()->route()->getPrefix() === '/admin')?'backend.templates.app':'frontend.templates.app')

@section('content')
    <div class="container">
        <div class="col-12">
            <h1 class="text-center mt-3"><b>ADMIN MODULE</b></h1><hr>
            <a href="/admin/modules/create" type="submit" class="btn btn-outline-dark mr-5 float-right">Create Module</a><br>
            <div class="row mt-lg-5">
                <div class="col-10 m-auto">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th scope="col">Alias</th>
                            <th scope="col">Route</th>
                            <th scope="col">Icon</th>
                            <th scope="col">Status</th>
                            <th scope="col">Created Time</th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($modules as $module)
                        <tr>
                            <td>{{ $module->alias }}</td>
                            <td>{{ $module->route }}</td>
                            <td>{{ $module->icon }}</td>
                            <td>{{ $module->status }}</td>
                            <td>{{ $module->created_at }}</td>
                            <td>
                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                        <a href="/admin/modules/{{ $module->id }}" type="submit" class="btn btn-outline-dark">View</a>
                                        <a href="/admin/modules/{{ $module->id }}/edit" type="submit" class="btn btn-outline-dark">Edit</a>
                                        <form action="/admin/modules/{{ $module->id }}" method="post">
                                            @method('DELETE')
                                            @csrf
                                            <button type="submit" class="btn btn-outline-dark">Delete</button>
                                        </form>
                                </div>
                            </td>
                        </tr>
                        @empty
                            <tr >
                                <td class="text-center">No Data Available</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>

                @if ($errors->all())
                    <div class="alert alert-danger m-auto mt-2">
                        @foreach ($errors->all() as $error)
                            <ul>
                                <li style="padding-left: 10px">
                                    {{ $error }}
                                </li>
                            </ul>
                        @endforeach
                    </div>
                @endif
                @if (session('message'))
                    <div class="alert alert-dark m-auto mt-2">
                        {{ session('message') }}
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
