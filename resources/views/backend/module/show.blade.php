@extends((request()->route()->getPrefix() === '/admin')?'backend.templates.app':'frontend.templates.app')

@section('content')
    <div class="container">
        <div class="col-12">
            <h1 class="text-center mt-3"><b>SHOW MODULE</b></h1><hr>
            <div class="row mt-lg-5">
                <div class="col-6 m-auto">
                    <div class="card text-center">
                        <div class="card-header">
                            Admin Module
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">{{ $view_module->alias }}</h5>
                            <h5 class="card-title">{{ $view_module->route }}</h5>
                            <h5 class="card-title">{{ $view_module->icon }}</h5>
                            <a href="/admin/modules" type="submit" class="btn btn-outline-dark">Go Back</a>
                        </div>
                        <div class="card-footer text-muted">
                            {{ $view_module->created_at }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
