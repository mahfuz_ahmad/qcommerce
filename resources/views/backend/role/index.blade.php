@extends((request()->route()->getPrefix() === '/admin')?'backend.templates.app':'frontend.templates.app')

@section('content')
    <div class="container">
        <div class="col-12">
            <h1 class="text-center mt-3"><b>ADMIN ROLE</b></h1><hr>
            <a href="/admin/roles/create" type="submit" class="btn btn-outline-dark mr-5 float-right">Create Role</a><br>
            <div class="row mt-lg-5">
                <div class="col-8 m-auto">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Status</th>
                            <th scope="col">Created Time</th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($roles as $role)
                        <tr>
                            <th scope="row">1</th>
                            <td>{{ $role->name }}</td>
                            <td>{{ $role->status }}</td>
                            <td>{{ $role->created_at }}</td>
                            <td>
                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                        <a href="/admin/roles/{{ $role->id }}" type="submit" class="btn btn-outline-dark">View</a>
                                        <a href="/admin/roles/{{ $role->id }}/edit" type="submit" class="btn btn-outline-dark">Edit</a>
                                        <form action="/admin/roles/{{ $role->id }}" method="post">
                                            @method('DELETE')
                                            @csrf
                                            <button type="submit" class="btn btn-outline-dark" onclick="myFunction()">Delete</button>
                                        </form>
                                    @if (session('message'))
                                        <div id="snackbar">
                                            {{ session('message') }}
                                        </div>
                                    @endif
                                </div>
                            </td>
                        </tr>
                        @empty
                            <tr >
                                <td class="text-center">No Data Available</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                </div>
                @if ($errors->all())
                    <div class="alert alert-danger m-auto mt-2">
                        @foreach ($errors->all() as $error)
                            <ul>
                                <li style="padding-left: 10px">
                                    {{ $error }}
                                </li>
                            </ul>
                        @endforeach
                    </div>
                @endif
                @if (session('message'))
                    <div class="alert alert-dark m-auto mt-2">
                        {{ session('message') }}
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
