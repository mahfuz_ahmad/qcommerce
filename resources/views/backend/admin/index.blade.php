@extends((request()->route()->getPrefix() === '/admin')?'backend.templates.app':'frontend.templates.app')

@section('content')
    <div class="container">
        <div class="col-12">
            <h1 class="text-center mt-3"><b>ADMIN LIST</b></h1><hr>
            <a href="/admin/roles/create" type="submit" class="btn btn-outline-dark mr-5 float-right">Create Admin</a><br>
            <div class="row mt-lg-5">
                <div class="col-8 m-auto">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Email</th>
                            <th scope="col">Role</th>
                            <th scope="col">Created Time</th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($admins as $admin)
                            <tr>
                                <th scope="row">1</th>
                                <td>{{ $admin->email }}</td>
                                <td>{{ $admin->role_id }}</td>
                                <td>{{ $admin->created_at }}</td>
                                <td>
                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                        <a href="/admin/roles/{{ $admin->id }}" type="submit" class="btn btn-outline-dark">View</a>
                                        <a href="/admin/roles/{{ $admin->id }}/edit" type="submit" class="btn btn-outline-dark">Edit</a>
                                        <form action="/admin/roles/{{ $admin->id }}" method="post">
                                            @method('DELETE')
                                            @csrf
                                            <button type="submit" class="btn btn-outline-dark">Delete</button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td>No Data Available</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
@endsection
