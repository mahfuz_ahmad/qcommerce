@extends((request()->route()->getPrefix() === '/admin')?'backend.templates.app':'frontend.templates.app')

@section('content')
    <div class="container">
        <div class="col-12">
            <h1 class="text-center mt-3"><b>SIGN UP</b></h1><hr>
            <div class="row mt-lg-5">
                <div class="col-6 m-auto">
                    <div class="card text-center">
                        <div class="card-header">
                            Sign up Your Account
                        </div>
                        <form action="/admin/auths" method="post">
                            @csrf
                            <div class="card-body">
                                <h5 class="card-title">
                                    <input type="email" name="email" class="form-control" placeholder="Enter Valid Email..">
                                </h5>
                                <h5 class="card-title">
                                    <input type="password" name="password" class="form-control" placeholder="Enter Secure Password..">
                                </h5>

                                <button type="submit" class="btn btn-outline-dark">Sign up</button>

                            </div>
                        </form>
                        <div class="card-footer text-muted">
                            Have Account?
                            <a href="{{ route('admin_login') }}" class="">Sign in.</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
