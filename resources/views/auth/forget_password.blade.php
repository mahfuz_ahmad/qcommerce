@extends((request()->route()->getPrefix() === '/admin')?'backend.templates.app':'frontend.templates.app')

@section('content')
    <div class="container">
        <div class="col-12">
            <h1 class="text-center mt-3"><b>FORGET PASSWORD</b></h1><hr>
            <div class="row mt-lg-5">
                <div class="col-6 m-auto">
                    <div class="card text-center">
                        <div class="card-header">
                            Verify your Email
                        </div>
                        <form action="/admin/send/email" method="post">
                            @csrf
                            <div class="card-body">
                                <p class="card-text">Please enter your Email and verify your email for create new password.</p>
                                <h5 class="card-title">
                                    <input type="email" name="email" class="form-control" placeholder="Enter your Email..">
                                </h5>
                                <button type="submit" class="btn btn-outline-dark">Send</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
