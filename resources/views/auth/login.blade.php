@extends((request()->route()->getPrefix() === '/admin')?'backend.templates.app':'frontend.templates.app')

@section('content')
    <div class="container">
        <div class="col-12">
            <h1 class="text-center mt-3"><b>SIGN IN</b></h1><hr>
            <div class="row mt-lg-5">
                <div class="col-6 m-auto">
                    <div class="card text-center">
                        <div class="card-header">
                            Sign in Your Account
                        </div>
                        <form action="/admin/checkLogin" method="post">
                            @csrf
                            <div class="card-body">
                                <input type="hidden" name="role_id" class="form-control" value="0">
                                <h5 class="card-title">
                                    <input type="email" name="email" class="form-control" placeholder="Enter Valid Email..">
                                </h5>
                                <h5 class="card-title">
                                    <input type="password" name="password" class="form-control" placeholder="Enter Secure Password..">
                                </h5>

                                <button type="submit" class="btn btn-outline-dark">Sign in</button>
                                <p class="mt-2">
                                    <a href="{{ route('forget_password') }}" class="">Forget Password?</a>
                                </p>
                            </div>
                        </form>
                        <div class="card-footer text-muted">
                            Don't Have Account?
                            <a href="{{ route('register') }}" class="">Sign up.</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
