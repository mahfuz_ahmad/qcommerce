@extends((request()->route()->getPrefix() === '/admin')?'backend.templates.app':'frontend.templates.app')

@section('content')
    <div class="container">
        <div class="col-12">
            <h1 class="text-center mt-3"><b>RESET PASSWORD</b></h1><hr>
            <div class="row mt-lg-5">
                <div class="col-6 m-auto">
                    <div class="card text-center">
                        <div class="card-header">
                            Reset your Password
                        </div>
                        <form action="/admin/update/password/{{ $admin_id->id }}" method="post">
                            @method('PUT')
                            @csrf
                            <div class="card-body">
                                <h5 class="card-title">
                                    <input type="password" name="password" class="form-control" placeholder="Enter your Password..">
                                </h5>
                                <h5 class="card-title">
                                    <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm your Password..">
                                </h5>
                                <button type="submit" class="btn btn-outline-dark">Update Password</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
