<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $fillable = [
        'alias',
        'route',
        'icon',
        'status',
        'deleted_at'
    ];
}
