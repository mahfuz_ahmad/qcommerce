<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailToken extends Model
{
    protected $fillable =
        [
            'email', 'type', 'email_verified_token'
        ];
}
