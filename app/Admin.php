<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Admin extends Authenticatable
{
    use Notifiable;

    protected $guard = 'admin';
    
    protected $fillable = [
        'role_id',
        'email',
        'password',
        'email_verified',
        'session',
        'status',
        'remember_token',
        'deleted_at',
    ];
    
    protected $hidden = [
        'password', 
        'remember_token',
    ];

    public function role()
    {
        return $this->hasOne('App\Role', 'id', 'role_id');
    }
}
