<?php

namespace App\Http\Controllers;

use App\Module;
use Carbon\Carbon;
use Illuminate\Http\Request;
use function Couchbase\basicDecoderV1;

class ModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modules = Module::all();
        return view('backend.module.index')
            ->with('modules', $modules);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.module.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'alias' => 'required|unique:modules',
            'route' => 'required|unique:modules',
            'icon' => 'required|unique:modules',
            'status' => 'required'
        ]);
        $module = new Module;
        $module->alias       = $request->alias;
        $module->route       = $request->route;
        $module->icon       = $request->icon;
        $module->status     = $request->status;
        $module->created_at = Carbon::now();
        $module->save();
        return back()
            ->withMessage('Module Created Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function show(Module $module)
    {
        $view_module = Module::find($module->id);
        return view('backend.module.show')
            ->with('view_module', $view_module);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function edit(Module $module)
    {
        $view_module = Module::find($module->id);
        return view('backend.module.edit')
            ->with('view_module', $view_module);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Module $module)
    {
//        $request->validate([
//            'alias' => 'required'
//        ]);
//        if($request->alias != $module->alias){
//            $request->validate([
//                'alias' => 'unique:modules'
//            ]);
//        }

        $module->update([
            'alias' => $request->alias,
            'route' => $request->route,
            'icon' => $request->icon,
            'status' => $request->status,
            'updated_at' => Carbon::now()
        ]);
        return back()
            ->withMessage('Module Updated Successful!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function destroy(Module $module)
    {
        Module::find($module->id)
            ->delete();
        return back()
            ->withMessage('Module Deleted Successful!');
    }
}
