<?php

namespace App\Http\Controllers;

use App\Role;
use Carbon\Carbon;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
        return view('backend.role.index')
            ->with('roles', $roles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.role.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:roles',
            'status' => 'required'
        ]);
        $role = new Role;
        $role->name       = $request->name;
        $role->status     = $request->status;
        $role->created_at = Carbon::now();
        $role->save();
        return back()
            ->withMessage('Role Created Successfully!');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        $view_role = Role::find($role->id);
        return view('backend.role.show')
            ->with('view_role', $view_role);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        $view_role = Role::find($role->id);
        return view('backend.role.edit')
            ->with('view_role', $view_role);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {

        $request->validate([
            'name' => 'required'
        ]);
        if($request->name != $role->name){
            $request->validate([
                'name' => 'unique:roles'
            ]);

        }

        $role->update([
            'name' => $request->name,
            'status' => $request->status,
            'updated_at' => Carbon::now(),
        ]);
        return back()
            ->withMessage('Role Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        Role::find($role->id)
            ->delete();
        return back()
            ->withMessage('Role Deleted Successfully!');
    }
}
