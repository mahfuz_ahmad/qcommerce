<?php

namespace App\Http\Controllers;

use Auth;
use App\Admin;
use App\EmailToken;
use App\PasswordReset;
use App\Mail\PasswordResetEmail;
use App\Mail\AdminVerification;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;


class AuthController extends Controller
{
    /**
     * Display a login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        return view('auth.login');
    }

    /**
     * Confirm login
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function loginCheck(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|alphaNum|min:6'
        ]);

        $email = $request->email;
        $password = $request->password;
        if (Auth::guard('admin')->attempt(['email' => $email, 'password' => $password]))
        {
            return redirect()->intended('admin');
        }

        // $email = $request->email;
        // $password = $request->password;
        // $admin = Admin::where('email', $email)->first();
        // if(isset($admin)){
        //     if(Hash::check($password, $admin->password)){
        //         session([
        //             'uid' => $admin->id,
        //             'email_verified' => $admin->email_verified,
        //             'status' => $admin->status,
        //             'rc' => $admin->remember_token,
        //         ]);
        //         return redirect()->route('dashboard');
        //     } else {
        //         return back();
        //     }
        // } else {
        //     return back();
        // }
    }

    public function logout(Request $request){
        Auth::guard('admin')->logout();
        return redirect('admin/login');
    }
    /**
     * Send email verification link.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function send_email(Request $request)
    {
        $email = $request->email;
        $ran_token = str_random(32);

        $token = new PasswordReset();
        $token->email = $email;
        $token->token = $ran_token;
        $token->created_at = Carbon::now();
        $insert_email = $token->save();
        if ($insert_email){
            Mail::to($email)->send(new PasswordResetEmail($email, $ran_token));
            return view('message.password_reset_verification');
        }
        else{
            return back();
        }
    }

    /**
     * Display a Password Reset form.
     *
     * @param  string  $email
     * @param  string  $token
     * @return \Illuminate\Http\Response
     */
    public function password_reset($email, $token)
    {
        $reset_token = PasswordReset::where('email', $email)->first();
        $token_data = $reset_token->token;
        if ($token_data === $token){
            $reset_token->delete();
            return view('auth.reset_password');
        }
    }

    /**
     * Display a Password Reset form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update_password(Request $request, Admin $admin)
    {

    }

    /**
     * Display a registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function register()
    {
        return view('auth.register');
    }

    /**
     * Display a Forget Password view.
     *
     * @return \Illuminate\Http\Response
     */
    public function forget_password()
    {
        return view('auth.forget_password');
    }

    /**
     * Display a registration Success View.
     *
     * @return \Illuminate\Http\Response
     */
    public function success()
    {
        return view('auth.success');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'email' => 'required|unique:admins',
            'password' => 'required'
        ]);

        $admin = new Admin;
        $admin->role_id = 0;
        $admin->email = $request->email;
        $admin->password = bcrypt($request->password);
        $admin->email_verified = 0;
        $admin->created_at = Carbon::now();
        
        // Admins Save
        $insert = $admin->save();
        
        if (isset($insert)){
            $email = $request->email;
            $ran_token = str_random(32);

            $token = new EmailToken;
            $token->email = $email;
            $token->type = 1;
            $token->email_verified_token = $ran_token;
            $token->created_at = Carbon::now();
            
            // EmailTokens on Admin save
            $insert_token = $token->save();
            
            if ($insert_token){
                /**
                 * Send email verification token
                 * On admin save send token to the admin email to
                 * verify the email.
                 */
                Mail::to($email)->send(new AdminVerification($email, $ran_token));
            }
            else{
                return back();
            }
        }
        else{
            return back();
        }
        return view('message.registration_success');
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $email
     * @param  string  $token
     * @return \Illuminate\Http\Response
     */
    public function verify_email($email, $token)
    {
        $email_token = EmailToken::where('email', $email)->first();
        $token_data = $email_token->email_verified_token;
        if($token_data === $token){
            $admin = Admin::where('email', $email)->first();
            $admin->update([
               'email_verified' => 1,
            ]);
            $email_token->delete();
            return view('auth.login');
        } else {
            print_r("Error");
            exit;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \IlluminateHttp\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
