<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Redis;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here are all the web routes for the application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group.
|
*/

// Frontend
Route::get('/', 'FrontendController@index')->name('home_page');
Route::get('/about', 'FrontendController@about');
Route::get('/login', 'AuthController@login')->name('login');

// Backend
Route::prefix('admin')->group(function(){
    // Root
    Route::get('/', 'BackendController')->name('dashboard');

    // Auth
    Route::resource('auths', 'AuthController');

    Route::get('/login', 'AuthController@login')->name('admin_login');
    Route::post('/checkLogin', 'AuthController@loginCheck')->name('check_login');
    Route::get('/logout', 'AuthController@logout')->name('admin_login');
    Route::get('/register', 'AuthController@register')->name('register');

    Route::post('/send/email', 'AuthController@send_email')->name('send_email');
    Route::get('/email_verify/{email}/{token}', 'AuthController@verify_email')->name('verify_email');

    Route::get('/forget/password', 'AuthController@forget_password')->name('forget_password');
    Route::get('/password/reset/{email}/{token}', 'AuthController@password_reset');
    Route::post('/update/password/{admin_id}', 'AuthController@update_password');
    Route::get('/index', 'AuthController@index')->name('index');
    Route::post('/store', 'AuthController@store')->name('store');
    Route::post('/update/password', 'AuthController@update_password');

    Route::get('/success', 'AuthController@success')->name('success');

    // Roles
    Route::resource('roles', 'RoleController');
    Route::resource('modules', 'ModuleController');
});

// Test Route
Route::get('/test', function(){
    echo "ok";
});
